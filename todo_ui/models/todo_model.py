# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.addons.base.res.res_request import referenceable_models


class Tag(models.Model):
    """Model define tags for tasks"""
    _name = 'todo.task.tag'
    _description = 'To-Do Tag'
    _parent_store = True

    _parent_name = 'parent_id'

    name = fields.Char(size=40, translate=True)
    parent_id = fields.Many2one(
        'todo.task.tag', 'Parent Tag', ondelete='restrict',
    )
    parent_left = fields.Integer('Parent Left', index=True)
    parent_rigth = fields.Integer('Parent Rigth', index=True)

    child_ids = fields.One2many(
        'todo.task.tag', 'parent_id', 'Child Tags',
    )
    # Exercise add m2m to tasks
    task_ids = fields.Many2many('todo.task', String='Tasks')


class Stage(models.Model):
    """Model define Stages for tasks and declare differents types of fields"""
    _name = 'todo.task.stage'
    _description = 'To-Do Stage'
    _order = 'sequence,name'
    # String fields:
    name = fields.Char(size=40, translate=True)
    desc = fields.Text('Description')
    state = fields.Selection(
        [('draft', 'New'), ('open', 'Started'),
         ('done', 'Closed')], 'State'
    )
    docs = fields.Html('Documentation')
    # Numeric fields:
    sequence = fields.Integer('Sequence')
    perc_complete = fields.Float('% Complete', (3, 2))
    # Date fields:
    date_effective = fields.Date('Effective Date')
    date_changed = fields.Datetime('Last Cahnged')
    # Other fields:
    fold = fields.Boolean('Folded?')
    image = fields.Binary('Image')
    # Exercise add O2m to tasks
    tasks = fields.One2many('todo.task', 'stage_id',
                            String='Tasks in this stage')


class TodoTask(models.Model):
    """Model inherit which contains the relations between Tags and Stages"""
    _inherit = 'todo.task'
    # Relational fields
    stage_id = fields.Many2one('todo.task.stage', 'Stage')
    tag_ids = fields.Many2many('todo.task.tag', string='Tags')

    refers_to = fields.Reference(referenceable_models,
        # [('res.user', 'User'), ('res.partner', 'Partner')],
        'Refres To',
    )
