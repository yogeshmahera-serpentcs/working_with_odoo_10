{
    'name': 'User interface improvements to the To-Do app',
    'description': 'User friendly features.',
    'author': '@jorgescalona',
    'depends': ['todo_user'],
}
